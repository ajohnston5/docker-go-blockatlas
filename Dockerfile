FROM golang:1.17-alpine

ARG SSH_DIR=/root/.ssh

LABEL version="0.1"

RUN apk update && apk add --no-cache \
    make git build-base linux-headers \
    redis libzmq czmq pkgconfig \
    czmq-dev libc-dev gcc && \
    rm -rf /var/cache/apt/* && \
    mkdir -p ${SSH_DIR} && chmod 700 ${SSH_DIR} &&\
    git config --global url.ssh://git@gitlab.com/.insteadOf https://gitlab.com/ && \
    git config --global url.ssh://git@github.com/.insteadOf https://github.com/

CMD ["sh", "-c"]
